#include "oled.h"
#include "i2c.h"
const uint8_t chinese_array_12_test[]=
{
	0x00,0x00,0x00,0x00,0xFF,0x10,0x10,0x10,0x10,0x10,0x00,0x00,
	0x08,0x08,0x08,0x08,0x0F,0x08,0x08,0x08,0x08,0x08,0x08,0x00,/*"上",0*/

};
//上能电气 格式12*12
const uint8_t chinese_array_12[]=
{
	0x00,0x00,0x00,0x00,0xFF,0x10,0x10,0x10,0x10,0x10,0x00,0x00,
	0x08,0x08,0x08,0x08,0x0F,0x08,0x08,0x08,0x08,0x08,0x08,0x00,/*"上",0*/

	0x04,0xF6,0x55,0x54,0xF6,0x00,0xCF,0x14,0x12,0x91,0x18,0x00,
	0x00,0x0F,0x01,0x09,0x0F,0x00,0x07,0x0A,0x09,0x08,0x0E,0x00,/*"能",1*/

	0xFC,0x24,0x24,0x24,0xFF,0x24,0x24,0x24,0xFC,0x00,0x00,0x00,
	0x03,0x01,0x01,0x01,0x07,0x09,0x09,0x09,0x09,0x08,0x0E,0x00,/*"电",2*/

	0x08,0x24,0x2B,0x2A,0x2A,0x2A,0x2A,0x2A,0xEA,0x0A,0x02,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x04,0x0E,0x00,/*"气",3*/

};
//上能电气 格式13*13
const uint8_t chinese_array_13[]=
{
	0x00,0x00,0x00,0x00,0x00,0xFE,0x20,0x20,0x20,0x10,0x00,0x00,
	0x00,0x00,0x04,0x04,0x04,0x07,0x04,0x04,0x04,0x04,0x04,0x00,/*"上",0*/

	0x00,0xE8,0x6C,0x6B,0x6C,0xEC,0x40,0xBE,0x24,0xA4,0x38,0x00,
	0x00,0x06,0x01,0x01,0x09,0x06,0x00,0x0F,0x09,0x08,0x0C,0x00,/*"能",1*/

	0x00,0x00,0xF8,0x28,0x28,0xFF,0x28,0x28,0x28,0xF8,0x00,0x00,
	0x00,0x00,0x01,0x01,0x01,0x07,0x09,0x09,0x09,0x09,0x0E,0x00,/*"电",2*/

	0x00,0x10,0x08,0x36,0x34,0x34,0x34,0x34,0xEC,0x04,0x04,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x07,0x08,0x0C,0x00,/*"气",3*/
};
//上能电气 格式14*14
const uint8_t chinese_array_14[]=
{
	0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x20,0x20,0x20,0x20,0x20,0x00,0x00,
	0x20,0x20,0x20,0x20,0x20,0x20,0x3F,0x20,0x20,0x20,0x20,0x20,0x20,0x00,/*"上",0*/

	0x08,0xCC,0x4A,0x49,0x48,0xCC,0x18,0x00,0x9F,0x24,0x24,0x22,0xBA,0x00,
	0x00,0x3F,0x05,0x05,0x25,0x3F,0x00,0x00,0x1F,0x22,0x22,0x21,0x38,0x00,/*"能",1*/

	0x00,0xF8,0x48,0x48,0x48,0x48,0xFF,0x48,0x48,0x48,0x48,0xF8,0x00,0x00,
	0x00,0x07,0x02,0x02,0x02,0x02,0x1F,0x22,0x22,0x22,0x22,0x23,0x38,0x00,/*"电",2*/

	0x10,0x48,0x47,0x54,0x54,0x54,0x54,0x54,0x54,0x54,0xD4,0x04,0x04,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0F,0x10,0x38,0x00,/*"气",3*/


};
//上能电气 格式15*15
const uint8_t chinese_array_15[]=
{
	0x00,0x00,0x00,0x00,0x00,0x00,0xFE,0x40,0x40,0x40,0x20,0x00,0x00,0x00,
	0x00,0x00,0x10,0x10,0x10,0x10,0x1F,0x10,0x10,0x10,0x10,0x10,0x10,0x00,/*"上",0*/

	0x00,0x10,0xD8,0x54,0x52,0xD0,0x58,0x00,0x7E,0x48,0x48,0x44,0x60,0x00,
	0x00,0x00,0x3F,0x05,0x25,0x3F,0x00,0x00,0x3F,0x24,0x22,0x21,0x38,0x00,/*"能",1*/

	0x00,0x00,0xF0,0x50,0x50,0x50,0xFE,0x50,0x50,0x50,0xF0,0x00,0x00,0x00,
	0x00,0x00,0x07,0x02,0x02,0x02,0x3F,0x22,0x22,0x22,0x27,0x20,0x38,0x00,/*"电",2*/

	0x00,0x40,0x70,0x4C,0x4A,0x68,0x68,0x68,0x68,0xE8,0x18,0x04,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x07,0x18,0x20,0x38,0x00,/*"气",3*/

};
//汉字 上能电气 字体格式16*16
const uint8_t chinese_array[]=
{	0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x40,0x40,0x40,0x40,0x40,0x40,0x00,0x00,0x00,
	0x40,0x40,0x40,0x40,0x40,0x40,0x7F,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x00,/*"上",0*/

	0x08,0xCC,0x4A,0x49,0x48,0x4A,0xCC,0x18,0x00,0x7F,0x88,0x88,0x84,0x82,0xE0,0x00,
	0x00,0xFF,0x12,0x12,0x52,0x92,0x7F,0x00,0x00,0x7E,0x88,0x88,0x84,0x82,0xE0,0x00,/*"能",0*/

	0x00,0x00,0xF8,0x88,0x88,0x88,0x88,0xFF,0x88,0x88,0x88,0x88,0xF8,0x00,0x00,0x00,
	0x00,0x00,0x1F,0x08,0x08,0x08,0x08,0x7F,0x88,0x88,0x88,0x88,0x9F,0x80,0xF0,0x00,/*"电",0*/
	0x20,0x10,0x4C,0x47,0x54,0x54,0x54,0x54,0x54,0x54,0x54,0xD4,0x04,0x04,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0F,0x30,0x40,0xF0,0x00,/*"气",1*/

};
//数字0到9 ，字体格式 16*16，横坐标只占了8个像素点，纵坐标占了16个像素点
const uint8_t number_array[]=
 {


 	0x00,0xE0,0x10,0x08,0x08,0x10,0xE0,0x00,0x00,0x0F,0x10,0x20,0x20,0x10,0x0F,0x00,/*"0",0*/

 	0x00,0x00,0x10,0x10,0xF8,0x00,0x00,0x00,0x00,0x00,0x20,0x20,0x3F,0x20,0x20,0x00,/*"1",0*/

 	0x00,0x70,0x08,0x08,0x08,0x08,0xF0,0x00,0x00,0x30,0x28,0x24,0x22,0x21,0x30,0x00,/*"2",0*/


 	0x00,0x30,0x08,0x08,0x08,0x88,0x70,0x00,0x00,0x18,0x20,0x21,0x21,0x22,0x1C,0x00,/*"3",0*/


 	0x00,0x00,0x80,0x40,0x30,0xF8,0x00,0x00,0x00,0x06,0x05,0x24,0x24,0x3F,0x24,0x24,/*"4",0*/

 	0x00,0xF8,0x88,0x88,0x88,0x08,0x08,0x00,0x00,0x19,0x20,0x20,0x20,0x11,0x0E,0x00,    //        5


 	0x00,0xE0,0x10,0x88,0x88,0x90,0x00,0x00,0x00,0x0F,0x11,0x20,0x20,0x20,0x1F,0x00  , //       6

 	0x00,0x18,0x08,0x08,0x88,0x68,0x18,0x00,0x00,0x00,0x00,0x3E,0x01,0x00,0x00,0x00,/*"7",0*/

 	0x00,0x70,0x88,0x08,0x08,0x88,0x70,0x00,0x00,0x1C,0x22,0x21,0x21,0x22,0x1C,0x00,/*"8",0*/

 	0x00,0xF0,0x08,0x08,0x08,0x10,0xE0,0x00,0x00,0x01,0x12,0x22,0x22,0x11,0x0F,0x00,/*"9",0*/

 };
/**
  * @brief  Write_Command，写命令
  * @param  参考芯片手册
  *
	*
  * @retval 无
  */
static void Write_Command(unsigned char I2C_Command)//???
{

    uint8_t *pData;
    pData = &I2C_Command;
	HAL_I2C_Mem_Write(&hi2c1,OLED_ADDRESS,0x00,I2C_MEMADD_SIZE_8BIT,pData,1,10);
}


/**
  * @brief  WriteDat，写数据命令全屏填充
  * @param  无参考芯片手册
  *
	*
  * @retval 无
  */
static void WriteDat(unsigned char I2C_Data)//???
{

    uint8_t *pData;
    pData = &I2C_Data;
	HAL_I2C_Mem_Write(&hi2c1,OLED_ADDRESS,0x40,I2C_MEMADD_SIZE_8BIT,pData,1,100);
}
/**
  * @brief  OLED_Fill，全屏填充
  * @param  0xff，全屏点亮
  *         0x00,全屏变黑
	*
  * @retval 无
  */
void OLED_Fill(unsigned char fill_Data)
{
	unsigned char m,n;
	for(m=0;m<8;m++)
	{
		Write_Command(0xb0+m);
		Write_Command(0x01);
		Write_Command(0x10);
		for(n=0;n<129;n++)
		{
			WriteDat(fill_Data);
		}
	}
}

/**
  * @brief  OLED_SetPos，设置光标
  * @param  x,光标x位置 范围 0-128
	*		y，光标y位置 范围 0-7
  * @retval 无
  */
void OLED_SetPos(unsigned char x, unsigned char y) //设置起始点坐标
{
	Write_Command(0xb0+y);

	Write_Command(((x&0xf0)>>4)|0x10);  //设置列的显示位置 的高低字节
	Write_Command((x&0x0f)|0x01);
}
/**
  * @brief  OLED_Show_Number_16，显示数字，数字大小16*16
  * @param  x,光标x位置 范围 0-128
	*		y，光标y位置 范围 0-7
	*		number 需要显示的数字，范围0-9，
  * @retval 无
  */
void  OLED_Show_Number_16(u8 x,u8  y,u8  number )
{
	uint32_t i=0,j=0;
	OLED_SetPos(x,y);
	//显示数字的上半部分
	for(i=0;i<8;i++)
	{
		WriteDat(number_array[number*16+i]);
	}
	//显示数字的下半部分
	OLED_SetPos(x,y+1);
	for(i=8;i<16;i++)
	{
		WriteDat(number_array[number*16+i]);
	}
}
/**
  * @brief  OLED_Show_Logo，显示公司名字，上能电气，字体大小16*16
  * @param  x,光标x位置 范围 0-128
	*		y，光标y位置 范围 0-7
	*
  * @retval 无
  */
void OLED_Show_Logo(u8 x,u8  y)
{
	uint32_t i=0,j=0;

	OLED_SetPos(x,y);
	for(i=0;i<16;i++)
	{
		WriteDat(chinese_array[i]);
	}
	OLED_SetPos(x,y+1);
	for(i=16;i<32;i++)
	{
		WriteDat(chinese_array[i]);
	}

	OLED_SetPos(x+16,y);
	for(i=0;i<16;i++)
	{
		WriteDat(chinese_array[32+i]);
	}
	OLED_SetPos(x+16,y+1);
	for(i=16;i<32;i++)
	{
		WriteDat(chinese_array[32+i]);
	}

	OLED_SetPos(x+32,y);
	for(i=0;i<16;i++)
	{
		WriteDat(chinese_array[64+i]);
	}
	OLED_SetPos(x+32,y+1);
	for(i=16;i<32;i++)
	{
		WriteDat(chinese_array[64+i]);
	}

	OLED_SetPos(x+48,y);
	for(i=0;i<16;i++)
	{
		WriteDat(chinese_array[96+i]);
	}
	OLED_SetPos(x+48,y+1);
	for(i=16;i<32;i++)
	{
		WriteDat(chinese_array[96+i]);
	}
}

void OLED_Show_Logo_12(u8 x,u8  y)
{
	uint32_t i=0,j=0;

	OLED_SetPos(x,y);
	for(i=0;i<12;i++)
	{
		WriteDat(chinese_array_12[i]);
	}
	OLED_SetPos(x,y+1);
	for(i=12;i<24;i++)
	{
		WriteDat(chinese_array_12[i]);
	}

	OLED_SetPos(x+12,y);
	for(i=0;i<12;i++)
	{
		WriteDat(chinese_array_12[24+i]);
	}
	OLED_SetPos(x+12,y+1);
	for(i=12;i<24;i++)
	{
		WriteDat(chinese_array_12[24+i]);
	}

	OLED_SetPos(x+24,y);
	for(i=0;i<12;i++)
	{
		WriteDat(chinese_array_12[48+i]);
	}
	OLED_SetPos(x+24,y+1);
	for(i=12;i<24;i++)
	{
		WriteDat(chinese_array_12[48+i]);
	}

	OLED_SetPos(x+36,y);
	for(i=0;i<12;i++)
	{
		WriteDat(chinese_array_12[72+i]);
	}
	OLED_SetPos(x+36,y+1);
	for(i=12;i<24;i++)
	{
		WriteDat(chinese_array_12[72+i]);
	}
}
void OLED_Show_Logo_13(u8 x,u8  y)
{
	uint32_t i=0,j=0;

	OLED_SetPos(x,y);
	for(i=0;i<12;i++)
	{
		WriteDat(chinese_array_13[i]);
	}
	OLED_SetPos(x,y+1);
	for(i=12;i<24;i++)
	{
		WriteDat(chinese_array_13[i]);
	}

	OLED_SetPos(x+12,y);
	for(i=0;i<12;i++)
	{
		WriteDat(chinese_array_13[24+i]);
	}
	OLED_SetPos(x+12,y+1);
	for(i=12;i<24;i++)
	{
		WriteDat(chinese_array_13[24+i]);
	}

	OLED_SetPos(x+24,y);
	for(i=0;i<12;i++)
	{
		WriteDat(chinese_array_13[48+i]);
	}
	OLED_SetPos(x+24,y+1);
	for(i=12;i<24;i++)
	{
		WriteDat(chinese_array_13[48+i]);
	}

	OLED_SetPos(x+36,y);
	for(i=0;i<12;i++)
	{
		WriteDat(chinese_array_13[72+i]);
	}
	OLED_SetPos(x+36,y+1);
	for(i=12;i<24;i++)
	{
		WriteDat(chinese_array_13[72+i]);
	}
}
void OLED_Show_Logo_14(u8 x,u8  y)
{
	uint32_t i=0,j=0;

	OLED_SetPos(x,y);
	for(i=0;i<14;i++)
	{
		WriteDat(chinese_array_14[i]);
	}
	OLED_SetPos(x,y+1);
	for(i=14;i<28;i++)
	{
		WriteDat(chinese_array_14[i]);
	}

	OLED_SetPos(x+14,y);
	for(i=0;i<14;i++)
	{
		WriteDat(chinese_array_14[28+i]);
	}
	OLED_SetPos(x+14,y+1);
	for(i=14;i<28;i++)
	{
		WriteDat(chinese_array_14[28+i]);
	}

	OLED_SetPos(x+28,y);
	for(i=0;i<14;i++)
	{
		WriteDat(chinese_array_14[56+i]);
	}
	OLED_SetPos(x+28,y+1);
	for(i=14;i<28;i++)
	{
		WriteDat(chinese_array_14[56+i]);
	}

	OLED_SetPos(x+42,y);
	for(i=0;i<14;i++)
	{
		WriteDat(chinese_array_14[84+i]);
	}
	OLED_SetPos(x+42,y+1);
	for(i=14;i<28;i++)
	{
		WriteDat(chinese_array_14[84+i]);
	}
}

void OLED_Show_Logo_15(u8 x,u8  y)
{
	uint32_t i=0,j=0;

	OLED_SetPos(x,y);
	for(i=0;i<14;i++)
	{
		WriteDat(chinese_array_15[i]);
	}
	OLED_SetPos(x,y+1);
	for(i=14;i<28;i++)
	{
		WriteDat(chinese_array_15[i]);
	}

	OLED_SetPos(x+14,y);
	for(i=0;i<14;i++)
	{
		WriteDat(chinese_array_15[28+i]);
	}
	OLED_SetPos(x+14,y+1);
	for(i=14;i<28;i++)
	{
		WriteDat(chinese_array_15[28+i]);
	}

	OLED_SetPos(x+28,y);
	for(i=0;i<14;i++)
	{
		WriteDat(chinese_array_15[56+i]);
	}
	OLED_SetPos(x+28,y+1);
	for(i=14;i<28;i++)
	{
		WriteDat(chinese_array_15[56+i]);
	}

	OLED_SetPos(x+42,y);
	for(i=0;i<14;i++)
	{
		WriteDat(chinese_array_15[84+i]);
	}
	OLED_SetPos(x+42,y+1);
	for(i=14;i<28;i++)
	{
		WriteDat(chinese_array_15[84+i]);
	}
}


void oled_init()
{
	HAL_Delay(2000);
	Write_Command(0xAE);//set display display ON/OFF,AFH/AEH
	Write_Command(0x02);
	Write_Command(0x10);
	Write_Command(0x40);//set display start line:COM0
	Write_Command(0xB0);
	Write_Command(0x81);//set contrast control
	Write_Command(0xCF);


	Write_Command(0xA1);//entire display on: A4H:OFF/A5H:ON
	Write_Command(0xC8); //该指令控制显示方向显示方向0xc8或者0xc0
	//Write_Command(0xC0);

	Write_Command(0xAF);
	Write_Command(0xA7);//set normal/inverse display: A6H:normal/A7H:inverse

	Write_Command(0xA8);//set multiplex ratio
	Write_Command(0x3F);//1/64duty
	Write_Command(0xD3);//set display offset
	Write_Command(0x00);//
	Write_Command(0xD5);//set display clock divide ratio/oscillator frequency
	Write_Command(0x80);//105Hz
	Write_Command(0xD9);//Dis-charge /Pre-charge Period Mode Set
	Write_Command(0xF1);//
	Write_Command(0xDA);//Common Pads Hardware Configuration Mode Set
	Write_Command(0x12);//
	Write_Command(0xDB);//set vcomh deselect level
	Write_Command(0x40);//VCOM = β X VREF = (0.430 + A[7:0] X 0.006415) X VREF
	Write_Command(0xA4);
	Write_Command(0xA6);
	Write_Command(0xAF);//set display display ON/OFF,AEH/AFH

	//这两条指令是设置反显，取消注释即为打开反显，
	//Write_Command(0xAF);
	//Write_Command(0xA7);//set normal/inverse display: A6H:normal/A7H:inverse

	OLED_Fill(0x00);//全屏灭
	HAL_Delay(1000);
	//HAL_Delay(1000);

	OLED_Fill(0xFF);//全屏点亮
	HAL_Delay(1000);
	HAL_Delay(1000);
	OLED_Fill(0x00);//全屏灭

	OLED_Show_Number_16(0,0,1);
	OLED_Show_Number_16(8,0,6);
	//OLED_Show_Number_16(16,0,8);

	OLED_Show_Number_16(0,2,1);
	OLED_Show_Number_16(8,2,6);

	OLED_Show_Number_16(0,4,1);
	OLED_Show_Number_16(8,4,6);

	OLED_Show_Number_16(0,6,1);
	OLED_Show_Number_16(8,6,6);
	OLED_Show_Logo(30,0);
	OLED_Show_Logo(30,2);
	OLED_Show_Logo(30,4);
	OLED_Show_Logo(30,6);

	//OLED_Show_Logo_12(30,0);
	//OLED_Show_Logo_12(30,2);
	//OLED_Show_Logo_12(30,4);
	//OLED_Show_Logo_12(30,6);

	//OLED_Show_Logo_15(30,0);
	//OLED_Show_Logo_15(30,2);
	//OLED_Show_Logo_15(30,4);
	//OLED_Show_Logo_15(30,6);
	//OLED_show_single_chinese_12(30,0,0);
}
void oled_loop()
{
	OLED_Show_Number_16(0,0,1);
	OLED_Show_Number_16(8,0,6);
	//OLED_Show_Number_16(16,0,8);

	OLED_Show_Number_16(0,2,1);
	OLED_Show_Number_16(8,2,6);

	OLED_Show_Number_16(0,4,1);
	OLED_Show_Number_16(8,4,6);

	OLED_Show_Number_16(0,6,1);
	OLED_Show_Number_16(8,6,6);
	OLED_Show_Logo(30,0);
	OLED_Show_Logo(30,2);
	OLED_Show_Logo(30,4);
	OLED_Show_Logo(30,6);

	HAL_Delay(10000);
	OLED_Fill(0x00);//全屏灭
	OLED_Fill(0x00);//全屏灭

	OLED_Show_Number_16(0,0,1);
	OLED_Show_Number_16(8,0,2);
	//OLED_Show_Number_16(16,0,8);

	OLED_Show_Number_16(0,2,1);
	OLED_Show_Number_16(8,2,2);

	OLED_Show_Number_16(0,4,1);
	OLED_Show_Number_16(8,4,2);

	OLED_Show_Number_16(0,6,1);
	OLED_Show_Number_16(8,6,2);
	OLED_Show_Logo_12(30,0);
	OLED_Show_Logo_12(30,2);
	OLED_Show_Logo_12(30,4);
	OLED_Show_Logo_12(30,6);

	HAL_Delay(10000);
	OLED_Fill(0x00);//全屏灭
	OLED_Fill(0x00);//全屏灭
	OLED_Show_Number_16(0,0,1);
	OLED_Show_Number_16(8,0,3);
	//OLED_Show_Number_16(16,0,8);

	OLED_Show_Number_16(0,2,1);
	OLED_Show_Number_16(8,2,3);

	OLED_Show_Number_16(0,4,1);
	OLED_Show_Number_16(8,4,3);

	OLED_Show_Number_16(0,6,1);
	OLED_Show_Number_16(8,6,3);
	OLED_Show_Logo_13(30,0);
	OLED_Show_Logo_13(30,2);
	OLED_Show_Logo_13(30,4);
	OLED_Show_Logo_13(30,6);

	HAL_Delay(10000);
	OLED_Fill(0x00);//全屏灭
	OLED_Fill(0x00);//全屏灭
	OLED_Show_Number_16(0,0,1);
	OLED_Show_Number_16(8,0,4);
	//OLED_Show_Number_16(16,0,8);

	OLED_Show_Number_16(0,2,1);
	OLED_Show_Number_16(8,2,4);

	OLED_Show_Number_16(0,4,1);
	OLED_Show_Number_16(8,4,4);

	OLED_Show_Number_16(0,6,1);
	OLED_Show_Number_16(8,6,4);
	OLED_Show_Logo_14(30,0);
	OLED_Show_Logo_14(30,2);
	OLED_Show_Logo_14(30,4);
	OLED_Show_Logo_14(30,6);

	HAL_Delay(10000);
	OLED_Fill(0x00);//全屏灭
	OLED_Fill(0x00);//全屏灭
	OLED_Show_Number_16(0,0,1);
	OLED_Show_Number_16(8,0,5);
	//OLED_Show_Number_16(16,0,8);

	OLED_Show_Number_16(0,2,1);
	OLED_Show_Number_16(8,2,5);

	OLED_Show_Number_16(0,4,1);
	OLED_Show_Number_16(8,4,5);

	OLED_Show_Number_16(0,6,1);
	OLED_Show_Number_16(8,6,5);
	OLED_Show_Logo_15(30,0);
	OLED_Show_Logo_15(30,2);
	OLED_Show_Logo_15(30,4);
	OLED_Show_Logo_15(30,6);

	HAL_Delay(10000);
	OLED_Fill(0x00);//全屏灭
	OLED_Fill(0x00);//全屏灭
}
uint8_t u8x8_byte_hw_i2c(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
    /* u8g2/u8x8 will never send more than 32 bytes between START_TRANSFER and END_TRANSFER */
    static uint8_t buffer[128];
    static uint8_t buf_idx;
    uint8_t *data;

    switch (msg)
    {
    case U8X8_MSG_BYTE_INIT:
    {
        /* add your custom code to init i2c subsystem */
        MX_I2C1_Init(); //I2C初始化
    }
    break;

    case U8X8_MSG_BYTE_START_TRANSFER:
    {
        buf_idx = 0;
    }
    break;

    case U8X8_MSG_BYTE_SEND:
    {
        data = (uint8_t *)arg_ptr;

        while (arg_int > 0)
        {
            buffer[buf_idx++] = *data;
            data++;
            arg_int--;
        }
    }
    break;

    case U8X8_MSG_BYTE_END_TRANSFER:
    {
        if (HAL_I2C_Master_Transmit(&hi2c1, (OLED_ADDRESS), buffer, buf_idx, 1000) != HAL_OK)
            return 0;
    }
    break;

    case U8X8_MSG_BYTE_SET_DC:
        break;

    default:
        return 0;
    }

    return 1;
}

void delay_us(uint32_t time)
{
    uint32_t i = 8 * time;
    while (i--)
        ;
}

uint8_t u8x8_gpio_and_delay(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
    switch (msg)
    {
    case U8X8_MSG_DELAY_100NANO: // delay arg_int * 100 nano seconds
        __NOP();
        break;
    case U8X8_MSG_DELAY_10MICRO: // delay arg_int * 10 micro seconds
        for (uint16_t n = 0; n < 320; n++)
        {
            __NOP();
        }
        break;
    case U8X8_MSG_DELAY_MILLI: // delay arg_int * 1 milli second
        HAL_Delay(1);
        break;
    case U8X8_MSG_DELAY_I2C: // arg_int is the I2C speed in 100KHz, e.g. 4 = 400 KHz
        delay_us(5);
        break;                    // arg_int=1: delay by 5us, arg_int = 4: delay by 1.25us
    case U8X8_MSG_GPIO_I2C_CLOCK: // arg_int=0: Output low at I2C clock pin
        break;                    // arg_int=1: Input dir with pullup high for I2C clock pin
    case U8X8_MSG_GPIO_I2C_DATA:  // arg_int=0: Output low at I2C data pin
        break;                    // arg_int=1: Input dir with pullup high for I2C data pin
    case U8X8_MSG_GPIO_MENU_SELECT:
        u8x8_SetGPIOResult(u8x8, /* get menu select pin state */ 0);
        break;
    case U8X8_MSG_GPIO_MENU_NEXT:
        u8x8_SetGPIOResult(u8x8, /* get menu next pin state */ 0);
        break;
    case U8X8_MSG_GPIO_MENU_PREV:
        u8x8_SetGPIOResult(u8x8, /* get menu prev pin state */ 0);
        break;
    case U8X8_MSG_GPIO_MENU_HOME:
        u8x8_SetGPIOResult(u8x8, /* get menu home pin state */ 0);
        break;
    default:
        u8x8_SetGPIOResult(u8x8, 1); // default return value
        break;
    }
    return 1;
}
void u8g2Init(u8g2_t *u8g2)
{
	u8g2_Setup_ssd1306_i2c_128x64_noname_f(u8g2, U8G2_R0, u8x8_byte_hw_i2c, u8x8_gpio_and_delay); // 初始化 u8g2 结构体
	u8g2_InitDisplay(u8g2);                                                                       // 根据所选的芯片进行初始化工作，初始化完成后，显示器处于关闭状态
	u8g2_SetPowerSave(u8g2, 0);                                                                   // 打开显示器
	u8g2_ClearBuffer(u8g2);
}
